class Snippet < ApplicationRecord
    serialize :required_snippet_names, Array
    has_many :requirements, class_name: "Requirement", foreign_key: "snippet_id"
    belongs_to :required_by, class_name: "Snippet"
    has_many :required_snippets, :through => :requirements

    def get_required_code
        code = ''
        self.required_snippets.each do |required|
            code += required.get_required_code
        end
        code += self.code
        return code
    end

end

class Requirement < ActiveRecord::Base
   belongs_to :snippet, :foreign_key => "snippet_id", :class_name => "Snippet"
   belongs_to :required_snippet, :foreign_key => "required_snippet_id", :class_name => "Snippet"
end
