module ApplicationHelper
    def sidebar_links(chapter, parent)
        if (chapter.has_children?)
            link = link_to(">>", chapter_path(chapter))
            subtree = ""
            chapter.children.all.order(:order).each do |child|
                subtree << sidebar_links(child, "sidebar-#{chapter.id}")
            end
            re = <<EOD
            <div class="card card-inverse">
              <div class="card-header" role="tab" id="sidebar-#{chapter.id}">
                <h5 class="mb-0">
                  <a data-toggle="collapse" data-parent="##{parent}" href="#collapse-#{chapter.id}" aria-expanded="true" aria-controls="collapse-#{chapter.id}">
                    #{chapter.title} #{link}
                  </a>
                </h5>
              </div>
              <div id="collapse-#{chapter.id}" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    #{subtree}
                </div>
              </div>
            </div>
EOD
            return re
        else
            link = link_to(chapter.title, chapter_path(chapter))
            re = <<EOD
            <div class="card card-inverse">
              <div class="card-header" role="tab" id="sidebar-#{chapter.id}">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="##{parent}" href="#collapse-#{chapter.id}" aria-expanded="false" aria-controls="collapse-#{chapter.id}">
                    #{link}
                  </a>
                </h5>
              </div>
            </div>
EOD
            return re
        end
    end
end
