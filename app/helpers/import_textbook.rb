def add_subdirs(parent_dir, dirname, parent)
    # Add all xml files to the database recursively
    # Text are added to table Chapter and snippets are added to table Snippet
    current_dir = parent_dir + "/" + dirname
    Dir.chdir(current_dir)
    file_name = dirname + ".xml"
    content = File.read(file_name)
    new_chapter = add_text(dirname, content, parent)

    # Make sure that chapters are added in the correct order
    d = Dir.glob("*").sort
    d.each do |entry|
        if (entry == '.' || entry == "..")
        elsif (File::directory?(current_dir+"/"+entry))
            add_subdirs(current_dir, entry, new_chapter)
        elsif (entry != file_name)
            content = File.read(current_dir+"/"+entry)
            add_text(entry, content, new_chapter)
        end
    end
end

#def replace_tag(doc, tag, text)
#    doc.search(tag).each do |node|
#        node.after(text)
#    end
#end

def add_text(title, content, parent)
    # Do the tidying/parsing here; not point parsing every time the textbook is loaded
    # Can use the code from add_subdirs
    puts "----- Importing: " + title
    add_snippets(content)
    order = title[/([0-9]+)/]
    order = (order.nil?) ? 0 : order.to_i
    return Chapter.create! :title => title, :xml_content => content,
        :parent => parent, :order => order
end


def add_snippets(text)
    # Scan through the text and add snippets to database
    # Will add named snippets only
    xml_doc = Nokogiri::XML(text)
    xml_doc.search('SNIPPET').each do |snippet|
        name = snippet.search('NAME').text
        if name.blank?
            # Unnamed snippet won't be referenced elsewhere
            next
        end

        requires = snippet.search('REQUIRES').to_a.map{ |node| node.text }
        do_eval = (snippet.search('EVAL') != 'no')
        hide = (snippet.search('HIDE') == 'yes')
        example = snippet.search('EXAMPLE').text

        # Add scheme snippets
        code = snippet.search('SCHEME')
        if (!code.blank?)
            new_snippet = Snippet.create! :name => name, :eval => do_eval,
                :code => code.text, :hide => hide, :example => example,
                :language => 'scheme', :required_snippet_names => requires
        end

        # Add javascript snippets
        code = snippet.search('JAVASCRIPT')
        if (!code.blank?)
            new_snippet = Snippet.create! :name => name, :eval => do_eval,
                :code => code.text, :hide => hide, :example => example,
                :language => 'javascript', :required_snippet_names => requires
        end
    end
end

def find_snippet_requirements()
    Snippet.all.each do |snippet|
        lan = snippet.language
        required_names = snippet.required_snippet_names
        required_names.each do |name|
            required_snippet = Snippet.find_by(name: name, language: lan)
            if (required_snippet.nil?)
                puts "Warning: No snippet named " + name + " in " + lan
            else
                snippet.required_snippets << required_snippet
            end
        end
    end
end

def test_snippet_requirements()
    Snippet.all.each do |snippet|
        puts snippet.name + " requires" + snippet.required_snippets.to_a.to_s
        #gets
    end
end

def update_chapter_names
    Chapter.all.each do |chapter|
        xml_doc = Nokogiri::XML(chapter.xml_content)
        if (!xml_doc.at('NAME').nil?)
            xml_doc.search('SCHEME').remove
            chapter.title = chapter.order.to_s + ". " + xml_doc.at('NAME').text.tr("\t\n\r", '')
            chapter.save
        else
            chapter.title = chapter.title.tr(".xml",'').capitalize
            chapter.save
        end
    end
end

# Remember to change the dir here after moving to bitbucket

Dir.chdir("app/helpers")
add_subdirs(Dir.pwd, "book", nil)
find_snippet_requirements
#test_snippet_requirements
update_chapter_names
