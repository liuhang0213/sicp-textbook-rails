      <SUBSECTION>
        <LABEL NAME="sec:compound-procedures"></LABEL>
        <NAME>
          <SPLITINLINE><SCHEME>Compound Procedures</SCHEME><JAVASCRIPT>Functions</JAVASCRIPT></SPLITINLINE>
        </NAME>

            <TEXT>
              We have identified in 
              <SPLITINLINE><SCHEME>Lisp</SCHEME><JAVASCRIPT>JavaScript</JAVASCRIPT></SPLITINLINE>
              some of the elements that must appear in
              any powerful programming language:
              <UL>
                <LI>
                  Numbers and arithmetic operations are 
                  primitive data and 
              <SPLITINLINE><SCHEME>procedures</SCHEME><JAVASCRIPT>functions</JAVASCRIPT></SPLITINLINE>.
                </LI>
                <LI>
                  Nesting of combinations provides a means of 
                  combining operations.
                </LI>
                <LI>
                  Definitions that associate names with values provide a
                  limited means of abstraction.
                </LI>
              </UL>
            </TEXT>

        <SPLIT>
          <SCHEME>
            <TEXT>
              Now we will learn about
              <INDEX>procedure<SUBINDEX>definition of</SUBINDEX></INDEX>
              <EM>procedure definitions</EM>, a much more powerful abstraction
              technique by which a compound operation can be defined, given a name and then
              referred to as a unit.
            </TEXT>
            <TEXT>
              We begin by examining how to express the idea of <QUOTE>squaring.</QUOTE>  We
              might say, <QUOTE>To square something, multiply it by itself.</QUOTE>  This is
              expressed in our language as 
              <!-- \indcode*{square} -->
              <SNIPPET>
                <NAME>square_definition</NAME>
                <EXAMPLE>square_example</EXAMPLE>
                <SCHEME>
(define (square x) (* x x))
                </SCHEME>
              </SNIPPET>
              <SNIPPET HIDE="yes">
                <NAME>square_example</NAME>
                <SCHEME>
(square 14)
                </SCHEME>
              </SNIPPET>
              We can understand this in the following way:
              <SNIPPET EVAL="no">
                <SCHEME>
(define (square x)  (*     x   x))
^$\ \ \ \uparrow\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \uparrow\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \uparrow\ \ \ \ \ \ \ \ \ \uparrow\ \ \ \ \ \ \ \ \ \ \ \ \ \uparrow\ \ \ \ \ \ \ \ \uparrow$^
 ^To^    ^square something,^ ^multiply^ ^it^ ^by itself.^
                </SCHEME>
              </SNIPPET>
              <INDEX>compound procedure</INDEX>
              <INDEX>procedure<SUBINDEX>compound</SUBINDEX></INDEX>
              We have here a <EM>compound procedure</EM>, which has been given the name
              <SCHEMEINLINE>square</SCHEMEINLINE>.  
              The procedure represents the operation of multiplying
              something by itself.  The thing to be multiplied is given a local
              name, <SCHEMEINLINE>x</SCHEMEINLINE>, 
              which plays the same role that a pronoun plays in
              natural language.
              <INDEX>naming<SUBINDEX>of procedures</SUBINDEX></INDEX>
              <INDEX>procedure<SUBINDEX>naming (with <TT>define</TT>)</SUBINDEX></INDEX>
              <INDEX>procedure<SUBINDEX>creating with <TT>define</TT></SUBINDEX></INDEX>
              Evaluating the definition creates this
              compound procedure and associates it with the <NOBR>name 
                <SCHEMEINLINE>square</SCHEMEINLINE>.</NOBR><FOOTNOTE>Observe 
                that there are two different operations
                being combined here: we are creating the procedure, and we are giving
                it the name <SCHEMEINLINE>square</SCHEMEINLINE>.  
                It is possible, indeed important, to be
                able to separate these two notions<EMDASH/>to create procedures without
                naming them, and to give names to procedures that have already been
                created.  We will see how to do this in Section <REF NAME="sec:lambda"/>.</FOOTNOTE>

              <!-- \indsf*{define}[procedures@for procedures] 
                   \ind*{special forms (those marked <EM>ns</EM> are not in the IEEE Scheme standard)}[define@{\tt define}]
                -->
            </TEXT>
          </SCHEME>
          <JAVASCRIPT>
            <TEXT>
              Now we will learn about
              <INDEX>function<SUBINDEX>definition of</SUBINDEX></INDEX>
              <EM>function definitions</EM>, a much more powerful abstraction
              technique by which a compound operation can be given a name and then
              referred to as a unit.
            </TEXT>
            <TEXT>
              We begin by examining how to express the idea of <QUOTE>squaring.</QUOTE>  We
              might say, <QUOTE>To square something, multiply it by itself.</QUOTE>  This is
              expressed in our language with the expression
              <!-- \indcode*{square} -->
              <SNIPPET EVAL="no">
                <NAME>square_expression</NAME>
                <EXAMPLE>square_example</EXAMPLE>
                <JAVASCRIPT>
function (x) { return x * x; }
                </JAVASCRIPT>
              </SNIPPET>
              <SNIPPET HIDE="yes">
                <NAME>square_expression_example</NAME>
                <JAVASCRIPT>
function (x) { return x * x; }(14)
                </JAVASCRIPT>
              </SNIPPET>


              <SNIPPET HIDE="yes">
                <NAME>square_definition</NAME>
                <EXAMPLE>square_example</EXAMPLE>
                <JAVASCRIPT>
function square(x) { return x * x; }
                </JAVASCRIPT>
              </SNIPPET>
              <SNIPPET HIDE="yes">
                <NAME>square_example</NAME>
                <JAVASCRIPT>
square(14);
                </JAVASCRIPT>
              </SNIPPET>
              The keyword 
              <JAVASCRIPTINLINE>function</JAVASCRIPTINLINE>
              indicates that a <EM>function</EM> is being defined.
              Here, we use the word <QUOTE>function</QUOTE> in a pragmatic 
              sense<EMDASH/>as
              instructions how to compute a new value from given 
              values<EMDASH/> and not in the mathematical sense as a mapping from
              a domain to a co-domain.
              The function above represents the operation of multiplying
              something by itself. The thing to be multiplied is given a local
              name, <JAVASCRIPTINLINE>x</JAVASCRIPTINLINE>, 
              which plays the same role that a pronoun plays in
              natural language.
              <INDEX>naming<SUBINDEX>of functions</SUBINDEX></INDEX>
              <INDEX>function<SUBINDEX>naming (with <TT>function</TT>)</SUBINDEX></INDEX>
              <INDEX>function<SUBINDEX>creating with <TT>function</TT></SUBINDEX></INDEX>
              Evaluating the <EM>function expression</EM> creates this
              function.
            </TEXT>
          </JAVASCRIPT>
        </SPLIT>

        <SPLIT>
          <SCHEME>
            <TEXT>
              The general form of a procedure definition is
              <SNIPPET EVAL="no">
                <SCHEME>
(define (^$\langle \textit{name} \rangle$^ ^$\langle\textit{formal parameters}\rangle$^) ^$\langle \textit{body} \rangle$^)
                </SCHEME>
              </SNIPPET>
              <INDEX>name<SUBINDEX><ORDER>procedure</ORDER>of a procedure</SUBINDEX>
              </INDEX>
              <INDEX>procedure<SUBINDEX>name of</SUBINDEX></INDEX>
              The <LATEXINLINE>$\langle \textit{name}\rangle$</LATEXINLINE> 
              is a symbol to be associated with the procedure
              definition in the environment.<FOOTNOTE>Throughout this book, we will
                <INDEX>notation in this book
                  <SUBINDEX>italic symbols in expression syntax</SUBINDEX></INDEX>
                <INDEX>syntax
                  <SUBINDEX><ORDER>expressions</ORDER>of expressions, describing</SUBINDEX>
                </INDEX>
                describe the general syntax of expressions by using italic symbols
                delimited by angle brackets<EMDASH/>e.g., 
                <LATEXINLINE>$\langle \textit{name}\rangle$</LATEXINLINE><EMDASH/>to denote the
                <QUOTE>slots</QUOTE> in the expression to be filled in 
                when such an expression is actually used.</FOOTNOTE>
              The 
              <INDEX>procedure<SUBINDEX>formal parameters of</SUBINDEX></INDEX>
              <INDEX>formal parameters</INDEX>
              <LATEXINLINE>$\langle \textit{formal parameters}\rangle$</LATEXINLINE> are
              the names used within the body of the procedure to refer to the
              corresponding arguments of the procedure.  The
              <INDEX>procedure<SUBINDEX>body of</SUBINDEX></INDEX>
              <INDEX>body of a procedure</INDEX>
              <LATEXINLINE>$\langle \textit{body} \rangle$</LATEXINLINE> 
              is an expression that will yield the value of
              the procedure application when the formal parameters are replaced by
              the actual arguments to which the procedure is 
              applied.<FOOTNOTE>More
                <INDEX>\sequence of expressions<SUBINDEX>
                    <ORDER>procedures</ORDER>in procedure body</SUBINDEX>
                </INDEX>
                generally, the body of the procedure can be a sequence of expressions.
                In this case, the interpreter evaluates each expression in the
                sequence in turn and returns the value of the final expression as the
                value of the procedure application.
              </FOOTNOTE>
              The <LATEXINLINE>$\langle \textit{name} \rangle$</LATEXINLINE>
              and the <LATEXINLINE>$\langle \textit{formal parameters} \rangle$</LATEXINLINE>
              are grouped within 
              <INDEX>parentheses<SUBINDEX><ORDER>procedure</ORDER>in procedure definition
              </SUBINDEX></INDEX>
              parentheses, just as they
              would be in an actual call to the procedure being defined.
              <INDEX>procedure<SUBINDEX>definition of</SUBINDEX></INDEX>
            </TEXT>

            <TEXT>
              Having defined <SCHEMEINLINE>square</SCHEMEINLINE>, 
              we can now use it:
              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <SCHEME>
(square 21)
                </SCHEME>
              </SNIPPET>

              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <SCHEME>
(square (+ 2 5))
                </SCHEME>
              </SNIPPET>

              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <SCHEME>
(square (square 3))
                </SCHEME>
              </SNIPPET>
            </TEXT>
          </SCHEME>
          <JAVASCRIPT>
            <TEXT>
              The general form of a function expression is
              <SNIPPET EVAL="no">
                <JAVASCRIPT>
function (^$\langle\textit{formal parameters}\rangle$^) {^$\langle\textit{body}\rangle$^}
                </JAVASCRIPT>
              </SNIPPET>
              The 
              <INDEX>function<SUBINDEX>formal parameters of</SUBINDEX></INDEX>
              <INDEX>formal parameters</INDEX>
              <LATEXINLINE>$\langle\textit{formal parameters}\rangle$</LATEXINLINE> are
              the names used within the body of the function to refer to the
              corresponding arguments of the function.<FOOTNOTE>Throughout this book, we will
                <INDEX>notation in this book
                  <SUBINDEX>italic symbols in statement syntax</SUBINDEX></INDEX>
                <INDEX>syntax
                  <SUBINDEX><ORDER>statement</ORDER>of statements, describing</SUBINDEX>
                </INDEX>
                describe the general syntax of statements by using italic symbols
                delimited by angle brackets<EMDASH/>e.g., 
                <LATEXINLINE>$\langle \textit{formal parameters}\rangle$</LATEXINLINE><EMDASH/>
                to denote the
                <QUOTE>slots</QUOTE> in the statement to be filled in 
                when such a statement is actually used.</FOOTNOTE>
 If there are more than one
              formal parameter, they are separated by commas. The
              <INDEX>function<SUBINDEX>body of</SUBINDEX></INDEX>
              <INDEX>body of a function</INDEX>
              <LATEXINLINE>$\langle\textit{body}\rangle$</LATEXINLINE> 
              is a statement that will return the value of
              the function application. In the function definition above,
              the keyword
              <JAVASCRIPTINLINE>return</JAVASCRIPTINLINE>
              precedes the expression <JAVASCRIPTINLINE>x * x</JAVASCRIPTINLINE>,
              indicating that the function returns the result of evaluating the
              expression. In other words, the body of this function is a 
              <JAVASCRIPTINLINE>return</JAVASCRIPTINLINE> statement of the 
              form<FOOTNOTE>More
                <INDEX>sequence of expressions<SUBINDEX>
                    <ORDER>functions</ORDER>in function body</SUBINDEX>
                </INDEX>
                generally, the body of the function can consist of statements other
                than such <JAVASCRIPTINLINE>return</JAVASCRIPTINLINE>
                statements, which allows the programmer to control exactly when the result 
                is returned. For example, the <JAVASCRIPTINLINE>return</JAVASCRIPTINLINE>
                statement could be the last statement in a sequence of statements.
                In this case, the interpreter evaluates each expression in the
                sequence in turn and returns the value of the final return expression as the
                value of the function application.
              </FOOTNOTE>
              <SNIPPET EVAL="no">
                <JAVASCRIPT>
return ^$\langle\textit{expression}\rangle$^;
                </JAVASCRIPT>
              </SNIPPET>
              In order to evaluate the statement that forms the body of the function,
              the formal parameters are replaced by
              the actual arguments to which the function is applied.
              The <LATEXINLINE>$\langle \textit{formal parameters} \rangle$</LATEXINLINE> follow
              the keyword <LATEXINLINE>function</LATEXINLINE> and are enclosed
              in
              <INDEX>parentheses<SUBINDEX><ORDER>function</ORDER>in function definition
              </SUBINDEX></INDEX>
              parentheses.
              <INDEX>function<SUBINDEX>definition of</SUBINDEX></INDEX>
            </TEXT>
            <TEXT>
              Having defined the function,
              we can now use it in a function application expression.<FOOTNOTE>
              Just like with previous expressions, the semicolon indicates
              that the expression is to be taken as a statement.</FOOTNOTE>
              <SNIPPET>
                <REQUIRES>square_expression_application</REQUIRES>
                <JAVASCRIPT>
(function (x) { return x * x; }) (21);
                </JAVASCRIPT>
              </SNIPPET>
              The first pair of parentheses encloses the function expression. Just like with
              arithmetic expressions, the interpreter evaluates it, resulting in a function value.
              The second pair of parentheses indicate that the function value is to be applied
              to an argument, here 21.
            </TEXT>
            <TEXT>
              Just as with arithmetic expressions, it is useful to refer to a function by a name,
              using a variable statement.
              <SNIPPET>
                <JAVASCRIPT>
var square = function (x) { return x * x; };
                </JAVASCRIPT>
              </SNIPPET>
              Now, we can simply write
              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <JAVASCRIPT>
square(21);
                </JAVASCRIPT>
              </SNIPPET>
              in order to apply the function. Other examples of using the 
              <JAVASCRIPTINLINE>square</JAVASCRIPTINLINE>
              function are
              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <JAVASCRIPT>
square(2 + 5);
                </JAVASCRIPT>
              </SNIPPET>
              and
              <SNIPPET>
                <REQUIRES>square_definition</REQUIRES>
                <JAVASCRIPT>
square(square(3));
                </JAVASCRIPT>
              </SNIPPET>
          Statements of the form 
          <SNIPPET EVAL="no">
            <JAVASCRIPT>
var ^$\langle\textit{name}\rangle$^ = function (^$\langle\textit{formal parameters}\rangle$^) {^$\langle\textit{body}\rangle$^};
            </JAVASCRIPT>
          </SNIPPET>
          perform two tasks. Firstly, they create a function, and secondly they are given
          it a name. Naming functions is obviously very useful, and therefore these two
          steps are supported in JavaScript with a slightly more convenient notation:
              <SNIPPET EVAL="no">
                <JAVASCRIPT>
function ^$\langle\textit{name}\rangle$^ (^$\langle\textit{formal parameters}\rangle$^) {^$\langle\textit{body}\rangle$^}
                </JAVASCRIPT>
              </SNIPPET>
          which we call a <EM>function definition statement</EM> and which 
          has essentially the same meaning as the variable statement above.
          Thus, the variable statement above that defines the variable 
          <JAVASCRIPTINLINE>square</JAVASCRIPTINLINE> can be equivalently written as
              <SNIPPET>
                <JAVASCRIPT>
function square(x) { return x * x; };
                </JAVASCRIPT>
              </SNIPPET>
            </TEXT>
          </JAVASCRIPT>
        </SPLIT>

        <SPLIT>
          <SCHEME>
            <TEXT>
              We can also use <SCHEMEINLINE>square</SCHEMEINLINE>
              as a building block in defining other
              procedures.  For example, <LATEXINLINE>$x^2 +y^2$</LATEXINLINE> 
              can be expressed as
              <SNIPPET EVAL="no">
                <SCHEME>
(+ (square x) (square y))
                </SCHEME>
              </SNIPPET>
            </TEXT>
          </SCHEME>
          <JAVASCRIPT>
            <TEXT>
              We can also use <JAVASCRIPTINLINE>square</JAVASCRIPTINLINE> 
              as a building block in defining other
              functions.  For example, <LATEXINLINE>$x^2 +y^2$</LATEXINLINE> 
              can be expressed as 
              <SNIPPET EVAL="no">
                <JAVASCRIPT>
square(x) + square(y)
                </JAVASCRIPT>
              </SNIPPET>
            </TEXT>
          </JAVASCRIPT>
        </SPLIT>

        <SPLIT>
          <SCHEME>
            <TEXT>
              We can easily define a procedure
              <SCHEMEINLINE>sum-of-squares</SCHEMEINLINE>
              that, given any two numbers as arguments, produces the
              sum of their squares:
              <SNIPPET>
                <NAME>sum_of_squares</NAME>
                <REQUIRES>square_definition</REQUIRES>
                <SCHEME>
(define (sum-of-squares x y)
  (+ (square x) (square y)))

(sum-of-squares 3 4)
                </SCHEME>
                <SCHEMEOUTPUT>
25
                </SCHEMEOUTPUT>
              </SNIPPET>
              Now we can use <SCHEMEINLINE>sum-of-squares</SCHEMEINLINE> 
              as a building block in constructing further procedures:
              <SNIPPET>
                <NAME>f</NAME>
                <REQUIRES>sum_of_squares</REQUIRES>
                <SCHEME>
(define (f a)
  (sum-of-squares (+ a 1) (* a 2)))

(f 5)
                </SCHEME>
                <SCHEMEOUTPUT>
                  136
                </SCHEMEOUTPUT>
              </SNIPPET>

              <INDEX>compound procedure<SUBINDEX>used like primitive procedure</SUBINDEX>
              </INDEX>
              Compound procedures are used in exactly the same way as primitive
              procedures. Indeed, one could not tell by looking at the definition
              of <SCHEMEINLINE>sum-of-squares</SCHEMEINLINE> given above whether 
              <SCHEMEINLINE>square</SCHEMEINLINE> was built into
              the interpreter, like <SCHEMEINLINE>+</SCHEMEINLINE> and 
              <ScHEMEINLINE>*</ScHEMEINLINE>, 
              or defined as a compound procedure.
            </TEXT>
          </SCHEME>
          <JAVASCRIPT>
            <TEXT>
              We can easily define a function
              <JAVASCRIPTINLINE>sum_of_squares</JAVASCRIPTINLINE>
              that, given any two numbers as arguments, produces the
              sum of their squares:
              <SNIPPET>
                <NAME>sum_of_squares</NAME>
                <EXAMPLE>sum_of_squares_example</EXAMPLE>
                <REQUIRES>square_definition</REQUIRES>
                <JAVASCRIPT>
function sum_of_squares(x,y) {
   return square(x) + square(y);
}
                </JAVASCRIPT>
              </SNIPPET>
              <SNIPPET HIDE="yes">
                <NAME>sum_of_squares_example</NAME>
                <JAVASCRIPT>
sum_of_squares(3,4);
                </JAVASCRIPT>
              </SNIPPET>
              Now we can use <JAVASCRIPTINLINE>sum_of_squares</JAVASCRIPTINLINE>
              as a building block in constructing further functions:
              <SNIPPET>
                <NAME>f</NAME>
                <EXAMPLE>f_example</EXAMPLE>
                <REQUIRES>sum_of_squares</REQUIRES>
                <JAVASCRIPT>
function f(a) {
   return sum_of_squares(a + 1, a * 2);
}
                </JAVASCRIPT>
              </SNIPPET>
              <SNIPPET HIDE="yes">
                <NAME>f_example</NAME>
                <JAVASCRIPT>
f(5);
              </JAVASCRIPT>
              </SNIPPET>
              <INDEX>compound procedure<SUBINDEX>used like primitive procedure</SUBINDEX>
              </INDEX>
              The application of functions such as
              <JAVASCRIPTINLINE>sum_of_squares(3,4)</JAVASCRIPTINLINE>
              are<EMDASH/>after operator combinations and function expressions<EMDASH/>the third 
              kind of combination
              of expressions into larger expressions that we encounter. 
            </TEXT>
          </JAVASCRIPT>
        </SPLIT>
      </SUBSECTION>