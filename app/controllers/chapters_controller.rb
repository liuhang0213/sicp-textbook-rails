require 'cgi'

class ChaptersController < ApplicationController
    def index
    end

    def remove_tag_name(doc, tag)
        doc.search(tag).each do |node|
            node.after(node.children)
            node.remove
        end
    end

    def replace_tag(doc, tag, text)
        doc.search(tag).each do |node|
            node.after(text)
            node.remove
        end
    end

    def show
        @chapter = Chapter.find(params[:id])
        content = @chapter.xml_content

        # String substitutions
        content = content.gsub('\\LaTeX','$\\rm\\LaTeX$');
        content = content.gsub('^',''); # No longer needed for rendering latex inline

        xml_doc = Nokogiri::XML(content)

        # Remove comments
        xml_doc.xpath('//comment()').each { |comment| comment.remove }

        # Remove tags according to textbook version
        if (LANGUAGE_VERSION == "javascript")
            xml_doc.search('SCHEME').remove
            remove_tag_name(xml_doc, "JAVASCRIPT")
        else
            xml_doc.search('JAVASCRIPT').remove
            remove_tag_name(xml_doc, "SCHEME")
        end

        if (!xml_doc.at('NAME').nil?)
            @title = xml_doc.at('NAME').text
            xml_doc.at('NAME').remove
        end

        xml_doc.search('HISTORY').remove
        xml_doc.search('INDEX').remove
        xml_doc.search('LABEL').remove
        xml_doc.search('OMISSION').remove
        xml_doc.search('SCHEMEOUTPUT').remove
        xml_doc.search('SUBINDEX').remove
        remove_tag_name(xml_doc, "SPLITINLINE")

        # Using Mathjax, remove tag names
        remove_tag_name(xml_doc, "LATEX")
        remove_tag_name(xml_doc, "LATEXINLINE")

        # Renaming tags
        # divs
        xml_doc.search('ATTRIBUTION, CAPTION').each do |div|
                div['class'] = 'chapter-text-' + div.name
                div.name = 'div'
            end

        # spans
        xml_doc.search('AUTHOR, DATE, TITLE').each do |span|
                span['class'] = 'chapter-text-' + span.name
                span.name = 'span'
        end

        # Figure without img/with link to gif
        xml_doc.search('FIGURE').each do |figure|
            if (!figure['src'].nil?)
                figure.name = 'img'
            end
        end

        # Links
        xml_doc.search('LINK').each do |link|
            link.name = 'a'
            link['href'] = link['address']
        end

        # Headings
        xml_doc.search('SUBHEADING').each do |heading|
            heading.name = 'h2'
        end

        # Blockquote
        xml_doc.search('EPIGRAPH').each do |quote|
            quote.name = 'blockquote'
            quote['class'] = 'blockquote'
        end

        # p
        xml_doc.search("TEXT").each do |text|
            text.name = 'p'
        end


        # Formatting
        replace_tag(xml_doc, 'APOS', '\'')
        replace_tag(xml_doc, 'SPACE', ' ')
        replace_tag(xml_doc, 'EMDASH', '—')
        replace_tag(xml_doc, 'ENDASH', '–')
        xml_doc.search('SCHEMEINLINE, JAVASCRIPTINLINE').each do |inline|
            inline.name = 'kbd'
        end



        # Footnotes
        added_footnote = false
        footnote_count = 1
        xml_doc.search('FOOTNOTE').each do |footnote|
            a = xml_doc.create_element("a", '[' + footnote_count.to_s + ']',
                :class => "superscript",
                :id => "footnote-link-" + footnote_count.to_s,
                :href => "#footnote-" + footnote_count.to_s)
            footnote.next = a
            footnote.unlink

            div = xml_doc.create_element("div",
                :class => "footnote")
            a_foot = xml_doc.create_element("a", '[' + footnote_count.to_s + '] ',
                :class => "footnote-number",
                :id => "footnote-" + footnote_count.to_s,
                :href => "#footnote-link-" + footnote_count.to_s)
            div.add_child(a_foot)
            div.add_child(footnote)

            if (!added_footnote)
                line = xml_doc.create_element("hr")
                xml_doc.root.add_child(line)
                added_footnote = true
            end
            xml_doc.root.add_child(div)
            footnote_count += 1
        end

        # Snippets
        count = 0;
        xml_doc.search('SNIPPET').each do |snippet|
            replace_tag(snippet, 'NAME', '')
            replace_tag(snippet, 'TEXT', '')
            replace_tag(snippet, 'SCHEME_STATEMENT', '')
            replace_tag(snippet, 'JAVASCRIPT_STATEMENT', '')
            requires = snippet.search('REQUIRES').to_a.map{ |node| node.text }
            replace_tag(snippet, 'REQUIRES', '')
            replace_tag(snippet, 'EXAMPLE', '')
            if (snippet['EVAL'] == 'no')
                snippet.name = 'kbd'
                snippet['class'] = 'snippet'
            elsif (snippet['HIDE'] == 'yes' || snippet['SOLUTION'] == 'yes')
                snippet.remove
            else
                # Interactive snippet
                hidden_code = ''
                requires.each do |required_name|
                    required = Snippet.find_by(name: required_name,
                        language: LANGUAGE_VERSION)
                    if (!required.nil?)
                        hidden_code += required.get_required_code
                    else
                        puts "Warning: Cannot find " + LANGUAGE_VERSION + " snippet named "
                            + required_name + "."
                    end
                end

                code = CGI.unescapeHTML(snippet.children.to_html.strip.html_safe)
                snippet_div = xml_doc.create_element("div",
                    :class => "snippet", :id => "javascript_#{count}_div")
                snippet_event = "window.eval('#{hidden_code}');
                      new window.JavascriptConsole('#{code}',
                      'javascript_#{count}','', event,1.0);".tr("\t\n\r", '')

                # For prettyprint
                snippet_pre = xml_doc.create_element('pre', code, :class => 'prettyprint', :title => 'Evaluate Javascript expression',
                    :onclick => snippet_event)

                # For IDE
                get_param = IDE_PREFIX + '"' + Sanitize.clean(code) + '"&hidden="' + Sanitize.clean(hidden_code) + '"'
                snippet_ide = xml_doc.create_element('a', "[Open in IDE]",
                    :href => get_param, :class => 'snippet-ide-link btn btn-primary')
                snippet.next = snippet_div
                snippet_div.next = snippet_pre
                snippet_pre.next = snippet_ide
                count += 1;
                snippet.remove
            end
        end

        remove_tag_name(xml_doc, "NAME")

        @html_doc = Nokogiri::HTML::DocumentFragment.parse ""
    #      xml_doc.search('TEXT, EPIGRAPH, ABOUT, CAPTION, FOOTNOTE, REFERENCE, EXERCISE').each do |text|
    #          new_par = xml_doc.create_element("div", :class => text.name)
    #          new_par.add_child(text)
    #          @html_doc.add_child(new_par)
    #      end
        xml_doc.children.each do |text|
            new_par = xml_doc.create_element("div", :class => text.name)
            new_par.add_child(text)
            @html_doc.add_child(new_par)
        end

        @html_doc = @html_doc.to_html
        respond_to do |format|
            format.html # index.html.erb
        end
    end
end
