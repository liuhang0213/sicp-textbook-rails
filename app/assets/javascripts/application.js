// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery3
// require jquery_ujs
//= require codemirror
// require tether
// require bootstrap
//= require_tree .
// require turbolinks

// Turbolink fix: https://stackoverflow.com/questions/17600093/rails-javascript-not-loading-after-clicking-through-link-to-helper
var ready = function() {

    // Formatting sidebar
    hightlight_sidebar(chapter_id);

    $(window).scroll(function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 5) {
            next_link = $('a.scroll-next:last').attr('href');
            next_chapter = $(".next-page:last");
            last_chapter = next_chapter.parent();

            next_chapter.load(next_link + " .chapter-content",
                function(responseTxt, statusTxt, xhr){
                    // There should be a better way than regex to get chapter id
                    var id_regex = /[0-9]+$/;
                    chapter_id = id_regex.exec(next_link);
                    $(".chapter-content:last").before("<hr/>");
                    newpage_ready();
                }
            );

        }
    });
};

var newpage_ready = function() {
    hightlight_sidebar(chapter_id);
    PR.prettyPrint();
    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
}

function hightlight_sidebar(chapter_id) {
    $('.sidebar-active').removeClass('sidebar-active');
    var active_tab = $('#sidebar-'+chapter_id);
    active_tab.addClass('sidebar-active');
    var parent_tab = active_tab.parent();
    while (parent_tab.attr('id') != 'nav-sidebar') {
        parent_tab.collapse('show');
        parent_tab = parent_tab.parent();
    }
}

$(document).ready(ready);
$(document).on('page:change', ready);
