Rails.application.routes.draw do
  resources :snippets
  resources :chapters
  get 'chapter/index'
  mathjax 'mathjax'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
